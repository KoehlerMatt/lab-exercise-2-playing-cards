#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit
{
	HEARTS,
	DIAMONDS,
	CLUBS,
	SPADES
};

enum Rank

{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card 
{
	Rank rank;
	Suit suit;
};

int main()
{

	_getch();
	return 0;
}